/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.service;

import com.example.demo.interfaces.IAsignation;
import com.example.demo.interfaces.IStudent;
import com.example.demo.mapper.AsignationTestObject;
import com.example.demo.model.AsignationTestModel;
import com.example.demo.model.StudentObjectModel;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author juliancastillo
 */
@Service
public class QualificationApplicationService {

    @Autowired
    private IStudent iStudent;

    public Object postAsignation(AsignationTestObject asignationTestObject) {
        Map<String, Object> responses = new HashMap<>();
        UUID uuid = UUID.randomUUID();
       
        List<StudentObjectModel> listStudent = iStudent.findAll();
        for (int i = 0; i < listStudent.size(); i++) {
                StudentObjectModel studentObjectModel = listStudent.get(i);
                iStudent.save(studentObjectModel);
        }
       
       // responses.put("Almacenamiento de las respuestas correctas", asignationTestModel);
        return responses;
    }

}

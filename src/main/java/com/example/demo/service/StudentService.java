/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.service;

import com.example.demo.interfaces.IStudent;
import com.example.demo.mapper.StudentObject;
import com.example.demo.model.StudentObjectModel;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author juliancastillo
 */
@Service
public class StudentService {

    @Autowired
    private IStudent student;

    public Object postCreateStudent(StudentObject studentObject) {
        Map<String, Object> responses = new HashMap<>();
        UUID uuid = UUID.randomUUID();

        StudentObjectModel studentObjectModel = new StudentObjectModel();
      //  studentObjectModel.setId(uuid.toString());
        studentObjectModel.setAge(String.valueOf(studentObject.getAge()));
        studentObjectModel.setCity(studentObject.getCity());
        studentObjectModel.setName(studentObject.getName());
        studentObjectModel.setTimeZone(studentObject.getTimeZone());
        student.save(studentObjectModel);
        responses.put("Creacion Correcta Estudiante", studentObjectModel);
        return responses;
    }

}

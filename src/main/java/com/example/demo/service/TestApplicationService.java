/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.service;

import com.example.demo.interfaces.IAsignation;
import com.example.demo.interfaces.IEvaluated;
import com.example.demo.interfaces.IQuestion;
import com.example.demo.interfaces.IStudent;
import com.example.demo.interfaces.ITest;
import com.example.demo.mapper.AsignationTestObject;
import com.example.demo.mapper.EvaluationTestObject;
import com.example.demo.mapper.QualificatedTestObject;
import com.example.demo.model.AsignationTestModel;
import com.example.demo.model.EvaluationModel;
import com.example.demo.model.QuestionModel;
import com.example.demo.model.StudentObjectModel;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author juliancastillo
 */
@Service
public class TestApplicationService {

    @Autowired
    private IAsignation iAsignation;

    @Autowired
    private IStudent iStudent;

    @Autowired
    private IEvaluated iEvaluated;
    @Autowired
    private ITest iQuestionData;

    @Autowired
    private IQuestion iQuestion;

    public Object postAsignation(AsignationTestObject asignationTestObject) {
        Map<String, Object> responses = new HashMap<>();
        UUID uuid = UUID.randomUUID();
        ZoneId zoneId = ZoneId.of("America/Bogota");
        ZonedDateTime departureDate = ZonedDateTime.of(asignationTestObject.getAge(), asignationTestObject.getMount(), asignationTestObject.getDay(),
                asignationTestObject.getHour(), asignationTestObject.getMinute(), 0, 0, zoneId);
        //ZoneId zoneIdAmd = ZoneId.of("Asia/Hong_Kong");
        AsignationTestModel asignationTestModel = new AsignationTestModel();
      //  asignationTestModel.setId(uuid.toString());
        asignationTestModel.setIdTest(asignationTestObject.getIdTest());
        asignationTestModel.setTimeLocal(departureDate.toString());
        iAsignation.save(asignationTestModel);
        List<StudentObjectModel> listStudent = iStudent.findAll();
        for (int i = 0; i < listStudent.size(); i++) {
            if (!listStudent.get(i).getTimeZone().equals(null)) {
                StudentObjectModel studentObjectModel = listStudent.get(i);
                ZoneId zoneIdAm = ZoneId.of(listStudent.get(i).getTimeZone());
                studentObjectModel.setDateToTest(ZonedDateTime.ofInstant(departureDate.toInstant(), zoneIdAm).toString());
                studentObjectModel.setIdTest(uuid.toString());
                iStudent.save(studentObjectModel);
            } else {
                StudentObjectModel studentObjectModel = listStudent.get(i);
                studentObjectModel.setDateToTest(ZonedDateTime.ofInstant(departureDate.toInstant(), zoneId).toString());
                studentObjectModel.setIdTest(uuid.toString());
                iStudent.save(studentObjectModel);
            }
        }
        responses.put("Creacion Correcta Asignacion de Examen", asignationTestModel);
        return responses;
    }

    public Object postEvaluation(EvaluationTestObject evaluationTestObject) {
        Map<String, Object> responses = new HashMap<>();
        UUID uuid = UUID.randomUUID();
        EvaluationModel evaluationModel = new EvaluationModel();
    //    evaluationModel.setId(uuid.toString());
        evaluationModel.setIdTest(evaluationTestObject.getIdTest());
        evaluationModel.setIdTestStudent(evaluationTestObject.getIdTestStudent());
        String answer = "";
        for (int i = 0; i < evaluationTestObject.getQuestionAnswerTestObjects().size(); i++) {
            if (i == evaluationTestObject.getQuestionAnswerTestObjects().size() - 1) {
                answer = answer + evaluationTestObject.getQuestionAnswerTestObjects().get(i).getIdQuestion() + ":" + evaluationTestObject.getQuestionAnswerTestObjects().get(i).getAsnwer();
            } else {
                answer = answer + evaluationTestObject.getQuestionAnswerTestObjects().get(i).getIdQuestion() + ":" + evaluationTestObject.getQuestionAnswerTestObjects().get(i).getAsnwer() + ",";
            }
        }
        evaluationModel.setQuestionTestAnswer(answer);
        iEvaluated.save(evaluationModel);
        responses.put("Prueba Recibida", evaluationModel);
        return responses;
    }

    public Object postAnswer(QualificatedTestObject qualificatedTestObject) {
        Map<String, Object> responses = new HashMap<>();
     //   UUID uuid = UUID.randomUUID();
        Optional<EvaluationModel> evaluated = iEvaluated.findById(qualificatedTestObject.getIdTestAEvaluar());
        int pointTotal = 0;
        String[] parts = evaluated.get().getQuestionTestAnswer().split(",");
        for (int i = 0; i < parts.length; i++) {
            String[] part = parts[i].split(":");
            System.out.print("AA"+part[0]+"AA"+part[1]);
            Optional<QuestionModel> questionModel = iQuestion.findById(Integer.parseInt(part[0]));
            if (questionModel.get().getAnswer().equals(part[1])) {
                pointTotal= pointTotal+questionModel.get().getPoint();
            }
        }
        Optional<StudentObjectModel> studentObjectModel = iStudent.findById(Integer.parseInt(evaluated.get().getIdTestStudent()));
        studentObjectModel.get().setQualification(String.valueOf(pointTotal));
        iStudent.save(studentObjectModel.get());
        responses.put(
                "Pruebas Calificadas", studentObjectModel.get());
        return responses;
    }
}

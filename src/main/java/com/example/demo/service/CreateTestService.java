/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.service;

import com.example.demo.interfaces.IQuestion;
import com.example.demo.interfacesService.IquestionService;
import com.example.demo.mapper.TestObject;
import com.example.demo.model.TestObjectModel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.interfaces.ITest;
import com.example.demo.mapper.Question;
import com.example.demo.model.QuestionModel;
import java.util.ArrayList;
import java.util.UUID;

/**
 *
 * @author juliancastillo
 */
@Service
public class CreateTestService implements IquestionService {

    @Autowired
    private ITest iQuestionData;

    @Autowired
    private IQuestion iQuestion;

    public Object postCreate(TestObject testObject) {
        UUID uuid = UUID.randomUUID();
        Map<String, Object> responses = new HashMap<>();
        boolean validate = validatePoint(testObject.getQuestionPoint());
        if (validate) {
            TestObjectModel testObjectModel = new TestObjectModel();
            testObjectModel.setName(testObject.getName());
            testObjectModel.setQuestion(testObject.getQuestion());
            int res = save(testObjectModel);
            if (res == 0) {
                responses.put("Error", "Create Test");
            }
            List<QuestionModel> questionModels = new ArrayList<>();
            int i = 0;
            for (int j = i; j < testObject.getQuestionPoint().size(); j++) {
                QuestionModel questionModel = new QuestionModel();
                questionModel.setPoint(testObject.getQuestionPoint().get(j).getPoint());
                questionModel.setQuestionTest(testObject.getQuestionPoint().get(j).getQuestion());
                questionModel.setQuestionTestA(testObject.getQuestionPoint().get(j).getQuestionTestA());
                questionModel.setQuestionTestB(testObject.getQuestionPoint().get(j).getQuestionTestB());
                questionModel.setQuestionTestC(testObject.getQuestionPoint().get(j).getQuestionTestC());
                questionModel.setQuestionTestD(testObject.getQuestionPoint().get(j).getQuestionTestD());
                questionModel.setIdTest(testObjectModel.getId());
                questionModel.setAnswer(testObject.getQuestionPoint().get(j).getAnswer());
                res = saveQuestionModel(questionModel);
                if (res == 1) {
                    questionModels.add(questionModel);
                }
                i = j;
            };
            if (res == 1 && i == testObject.getQuestionPoint().size() - 1) {
                responses.put("Exitoso Creacion Test", testObjectModel);
                responses.put("Exitoso Crecion Formulario", questionModels);
            } else {
                responses.put("Error", "Create Test");
            }
        }else {
                responses.put("Error", "Los puntos de la suma de las preguntas deben sumar 100");
            }
        return responses;
    }

    @Override
    public Optional<Object> listId(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int save(Object t) {
        int res = 0;
        TestObjectModel testModel = (TestObjectModel) t;
        iQuestionData.save(testModel);
        if (!testModel.equals(null)) {
            res = 1;
        }
        return res;
    }

    @Override
    public List<Object> list() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int saveQuestionModel(QuestionModel t) {
        int res = 0;
        QuestionModel questionModels = (QuestionModel) t;
        if (!questionModels.equals(null)) {
            res = 1;
        }
        iQuestion.save(questionModels);
        return res;
    }

    private boolean validatePoint(List<Question> questionPoint) {

        int points = 100;
        int pointRecive = 0;
        for (int i = 0; i < questionPoint.size(); i++) {
            pointRecive = pointRecive + questionPoint.get(i).getPoint();
        }
        if (points == pointRecive) {
            return true;
        } else {
            return false;
        }
    }

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.mapper;

import java.util.List;

/**
 *
 * @author juliancastillo
 */
public class TestObject {

    private String name;
    private String question;
    private List<Question> questionPoint;
    

    public TestObject(String name, String question, List<Question> questionPoint) {
        this.name = name;
        this.question = question;
        this.questionPoint = questionPoint;
       
    }

    public TestObject() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<Question> getQuestionPoint() {
        return questionPoint;
    }

    public void setQuestionPoint(List<Question> questionPoint) {
        this.questionPoint = questionPoint;
    }

}

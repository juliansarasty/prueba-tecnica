/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.mapper;

import java.util.List;

/**
 *
 * @author juliancastillo
 */
public class QualificatedTestObject {

    private int idTestAEvaluar;

    public QualificatedTestObject(int idTestAEvaluar) {
        super();
        this.idTestAEvaluar = idTestAEvaluar;
    }

    public QualificatedTestObject() {
    }

    public int getIdTestAEvaluar() {
        return idTestAEvaluar;
    }

    public void setIdTestAEvaluar(int idTestAEvaluar) {
        this.idTestAEvaluar = idTestAEvaluar;
    }

   


}

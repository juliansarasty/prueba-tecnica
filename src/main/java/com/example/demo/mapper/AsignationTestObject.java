/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.mapper;

/**
 *
 * @author juliancastillo
 */
public class AsignationTestObject {

    private int age;
    private int mount;
    private int day;
    private int hour;
    private int minute;
    private String idTest;

    public AsignationTestObject(int age, int mount, int day, int hour, int minute, String idTest) {
        this.age = age;
        this.mount = mount;
        this.day = day;
        this.hour = hour;
        this.minute = minute;
        this.idTest = idTest;
    }

    public AsignationTestObject() {
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getMount() {
        return mount;
    }

    public void setMount(int mount) {
        this.mount = mount;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public String getIdTest() {
        return idTest;
    }

    public void setIdTest(String idTest) {
        this.idTest = idTest;
    }

}

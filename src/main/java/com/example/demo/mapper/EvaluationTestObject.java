/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.mapper;

import java.util.List;

/**
 *
 * @author juliancastillo
 */
public class EvaluationTestObject {

    private String idTest;
    private String idTestStudent;
    private List<QuestionAnswerTestObject> questionAnswerTestObjects;

    public EvaluationTestObject(String idTest, String idTestStudent, List<QuestionAnswerTestObject> questionAnswerTestObjects) {
        super();
        this.idTest = idTest;
        this.idTestStudent = idTestStudent;
        this.questionAnswerTestObjects = questionAnswerTestObjects;
    }
       
    public EvaluationTestObject() {
    }

    public List<QuestionAnswerTestObject> getQuestionAnswerTestObjects() {
        return questionAnswerTestObjects;
    }

    public void setQuestionAnswerTestObjects(List<QuestionAnswerTestObject> questionAnswerTestObjects) {
        this.questionAnswerTestObjects = questionAnswerTestObjects;
    }

    public String getIdTest() {
        return idTest;
    }

    public void setIdTest(String idTest) {
        this.idTest = idTest;
    }

    public String getIdTestStudent() {
        return idTestStudent;
    }

    public void setIdTestStudent(String idTestStudent) {
        this.idTestStudent = idTestStudent;
    }
   
    
    

}

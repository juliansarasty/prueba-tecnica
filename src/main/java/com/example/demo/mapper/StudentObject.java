/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.mapper;

/**
 *
 * @author juliancastillo
 */
public class StudentObject {

    private String name;
    private int age;
    private String city;
    private String timeZone;


    public StudentObject() {
    }

    public StudentObject(String name, int age, String city, String timeZone) {
        this.name = name;
        this.age = age;
        this.city = city;
        this.timeZone = timeZone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    @Override
    public String toString() {
        return "StudentObject{" + "name=" + name + ", age=" + age + ", city=" + city + ", timeZone=" + timeZone + '}';
    }

   
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.mapper;

/**
 *
 * @author juliancastillo
 */
public class Question {

    private String question;
    private int point;
    private String answer;
    private String questionTestA;
    private String questionTestB;
    private String questionTestC;
    private String questionTestD;

    public Question() {
    }

    public Question(String question, int point, String answer, String questionTestA, String questionTestB, String questionTestC, String questionTestD) {
        super();
        this.question = question;
        this.point = point;
        this.answer = answer;
        this.questionTestA = questionTestA;
        this.questionTestB = questionTestB;
        this.questionTestC = questionTestC;
        this.questionTestD = questionTestD;
    }

    public String getQuestionTestA() {
        return questionTestA;
    }

    public void setQuestionTestA(String questionTestA) {
        this.questionTestA = questionTestA;
    }

    public String getQuestionTestB() {
        return questionTestB;
    }

    public void setQuestionTestB(String questionTestB) {
        this.questionTestB = questionTestB;
    }

    public String getQuestionTestC() {
        return questionTestC;
    }

    public void setQuestionTestC(String questionTestC) {
        this.questionTestC = questionTestC;
    }

    public String getQuestionTestD() {
        return questionTestD;
    }

    public void setQuestionTestD(String questionTestD) {
        this.questionTestD = questionTestD;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

}

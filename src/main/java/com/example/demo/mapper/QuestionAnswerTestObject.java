/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.mapper;

/**
 *
 * @author juliancastillo
 */
public class QuestionAnswerTestObject {

    private String idQuestion;
    private String asnwer;

    public QuestionAnswerTestObject(String idQuestion, String asnwer) {
        super();
        this.idQuestion = idQuestion;
        this.asnwer = asnwer;
    }

    public QuestionAnswerTestObject() {
    }

    public String getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(String idQuestion) {
        this.idQuestion = idQuestion;
    }

    public String getAsnwer() {
        return asnwer;
    }

    public void setAsnwer(String Asnwer) {
        this.asnwer = Asnwer;
    }

}

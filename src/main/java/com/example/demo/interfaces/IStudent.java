/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.demo.interfaces;

import com.example.demo.model.StudentObjectModel;
import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author juliancastillo
 */
@Repository
public interface IStudent extends CrudRepository<StudentObjectModel, Integer> {

    List<StudentObjectModel> findAll();

    public void save(Optional<StudentObjectModel> studentObjectModel);
}

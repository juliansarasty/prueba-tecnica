/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.controller;

import com.example.demo.mapper.AsignationTestObject;
import com.example.demo.mapper.EvaluationTestObject;
import com.example.demo.mapper.QualificatedTestObject;
import com.example.demo.mapper.StudentObject;
import com.example.demo.mapper.TestObject;
import com.example.demo.service.StudentService;
import com.example.demo.service.CreateTestService;
import com.example.demo.service.TestApplicationService;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author juliancastillo
 */
@RestController
public class DemoController {

    @Autowired
    CreateTestService createTestService;

    @Autowired
    StudentService createStudentService;

    @Autowired
    TestApplicationService testApplicationService;

    @GetMapping("/hello")
    public String hello() {
        return String.format("Hello world!");
    }

    @PostMapping("/createTest")
    public ResponseEntity<Map<String, Object>> createTest(@RequestBody TestObject testObject) throws JsonProcessingException {
        Map<String, Object> responses = new HashMap<>();
        Map<String, Object> error = new HashMap<>();
        try {
            return new ResponseEntity(createTestService.postCreate(testObject), HttpStatus.OK);

        } catch (Exception ex) {
            error.put("mensajeError", ex.getMessage());
            responses.put("status", false);
            responses.put("body", "");
            responses.put("error", error);
            return new ResponseEntity(responses, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/createStudent")
    public ResponseEntity<Map<String, Object>> createStudent(@RequestBody StudentObject studentObject) throws JsonProcessingException {
        Map<String, Object> responses = new HashMap<>();
        Map<String, Object> error = new HashMap<>();
        try {
            return new ResponseEntity(createStudentService.postCreateStudent(studentObject), HttpStatus.OK);
        } catch (Exception ex) {
            error.put("mensajeError", ex.getMessage());
            responses.put("status", false);
            responses.put("body", "");
            responses.put("error", error);
            return new ResponseEntity(responses, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/asignationTest")
    public ResponseEntity<Map<String, Object>> asignationTest(@RequestBody AsignationTestObject asignationTestObject) throws JsonProcessingException {
        Map<String, Object> responses = new HashMap<>();
        Map<String, Object> error = new HashMap<>();
        try {
            return new ResponseEntity(testApplicationService.postAsignation(asignationTestObject), HttpStatus.OK);
        } catch (Exception ex) {
            error.put("mensajeError", ex.getMessage());
            responses.put("status", false);
            responses.put("body", "");
            responses.put("error", error);
            return new ResponseEntity(responses, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/evaluationTestObject")
    public ResponseEntity<Map<String, Object>> asignationTest(@RequestBody EvaluationTestObject evaluationTestObject) throws JsonProcessingException {
        Map<String, Object> responses = new HashMap<>();
        Map<String, Object> error = new HashMap<>();
        try {
            return new ResponseEntity(testApplicationService.postEvaluation(evaluationTestObject), HttpStatus.OK);
        } catch (Exception ex) {
            error.put("mensajeError", ex.getMessage());
            responses.put("status", false);
            responses.put("body", "");
            responses.put("error", error);
            return new ResponseEntity(responses, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/checkAnswer")
    public ResponseEntity<Map<String, Object>> asignationTest(@RequestBody QualificatedTestObject qualificatedTestObject) throws JsonProcessingException {
        Map<String, Object> responses = new HashMap<>();
        Map<String, Object> error = new HashMap<>();
        try {
            return new ResponseEntity(testApplicationService.postAnswer(qualificatedTestObject), HttpStatus.OK);
        } catch (Exception ex) {
            error.put("mensajeError", ex.getMessage());
            responses.put("status", false);
            responses.put("body", "");
            responses.put("error", error);
            return new ResponseEntity(responses, HttpStatus.NOT_FOUND);
        }
    }
}

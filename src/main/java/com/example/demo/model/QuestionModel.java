/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.context.annotation.Primary;

/**
 *
 * @author juliancastillo
 */
@Entity
@Table(name = "Questions")
public class QuestionModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 50)
    private int idTest;

    @Column(length = 50)
    private String questionTest;
    @Column(length = 50)
    private String questionTestA;
    @Column(length = 50)
    private String questionTestB;
    @Column(length = 50)
    private String questionTestC;
    @Column(length = 50)
    private String questionTestD;
    @Column(length = 3)
    private int point;
    @Column(length = 1)
    private String answer;

    public int getIdTest() {
        return idTest;
    }

    public void setIdTest(int idTest) {
        this.idTest = idTest;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public QuestionModel() {
    }

  

    public String getQuestionTest() {
        return questionTest;
    }

    public void setQuestionTest(String questionTest) {
        this.questionTest = questionTest;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getQuestionTestA() {
        return questionTestA;
    }

    public void setQuestionTestA(String questionTestA) {
        this.questionTestA = questionTestA;
    }

    public String getQuestionTestB() {
        return questionTestB;
    }

    public void setQuestionTestB(String questionTestB) {
        this.questionTestB = questionTestB;
    }

    public String getQuestionTestC() {
        return questionTestC;
    }

    public void setQuestionTestC(String questionTestC) {
        this.questionTestC = questionTestC;
    }

    public String getQuestionTestD() {
        return questionTestD;
    }

    public void setQuestionTestD(String questionTestD) {
        this.questionTestD = questionTestD;
    }

    public QuestionModel(int id, int idTest, String questionTest, String questionTestA, String questionTestB, String questionTestC, String questionTestD, int point, String answer) {
        super();
        this.id = id;
        this.idTest = idTest;
        this.questionTest = questionTest;
        this.questionTestA = questionTestA;
        this.questionTestB = questionTestB;
        this.questionTestC = questionTestC;
        this.questionTestD = questionTestD;
        this.point = point;
        this.answer = answer;
    }

}

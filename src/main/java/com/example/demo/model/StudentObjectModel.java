/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author juliancastillo
 */
@Entity
@Table(name = "StudentTestObject")
public class StudentObjectModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 50)
    private String name;
    @Column(length = 3)
    private String age;
    @Column(length = 50)
    private String city;
    @Column(length = 50)
    private String timeZone;
    @Column(name = "UUIDTest")
    private String idTest;
    @Column(length = 150)
    private String dateToTest;

    @Column(length = 3)
    private String qualification;

    public StudentObjectModel(int id, String name, String age, String city, String timeZone, String idTest, String dateToTest, String qualification) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.city = city;
        this.timeZone = timeZone;
        this.idTest = idTest;
        this.dateToTest = dateToTest;
        this.qualification = qualification;
    }

    public StudentObjectModel() {
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getIdTest() {
        return idTest;
    }

    public void setIdTest(String idTest) {
        this.idTest = idTest;
    }

    public String getDateToTest() {
        return dateToTest;
    }

    public void setDateToTest(String dateToTest) {
        this.dateToTest = dateToTest;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

}

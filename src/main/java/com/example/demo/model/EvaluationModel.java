/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author juliancastillo
 */
@Entity
@Table(name = "Evaluations")
public class EvaluationModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "UUIDTest")
    private String idTest;
    @Column(name = "UUIDStudent")
    private String idTestStudent;
    @Column(length = 2000)
    private String questionTestAnswer;

    public EvaluationModel(int id, String idTest, String idTestStudent, String questionTestAnswer) {
        super();
        this.id = id;
        this.idTest = idTest;
        this.idTestStudent = idTestStudent;
        this.questionTestAnswer = questionTestAnswer;
    }

    public EvaluationModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdTest() {
        return idTest;
    }

    public void setIdTest(String idTest) {
        this.idTest = idTest;
    }

    public String getIdTestStudent() {
        return idTestStudent;
    }

    public void setIdTestStudent(String idTestStudent) {
        this.idTestStudent = idTestStudent;
    }

    public String getQuestionTestAnswer() {
        return questionTestAnswer;
    }

    public void setQuestionTestAnswer(String questionTestAnswer) {
        this.questionTestAnswer = questionTestAnswer;
    }

}

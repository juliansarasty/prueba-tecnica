package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Service;

import com.example.demo.controller.DemoController;
import com.example.demo.mapper.AsignationTestObject;
import com.example.demo.mapper.EvaluationTestObject;
import com.example.demo.mapper.QualificatedTestObject;
import com.example.demo.mapper.Question;
import com.example.demo.mapper.QuestionAnswerTestObject;
import com.example.demo.mapper.StudentObject;
import com.example.demo.mapper.TestObject;
import com.example.demo.service.CreateTestService;
import com.example.demo.service.StudentService;
import com.example.demo.service.TestApplicationService;
import com.fasterxml.jackson.core.JsonProcessingException;

@SpringBootTest
class DemoApplicationTests {

	 @Autowired
	private CreateTestService createTestService;

	@Autowired
	private StudentService studentService ;
	@Autowired
	private TestApplicationService testApplicationService ;
	@Test
	void contextLoads() {
		
	
	}
	@Test
	public void createTest() {
		 TestObject testObject = new TestObject();
	     String name ="nameTest";
	     String question ="this is the question a";
	     Question questionPoint = new Question();
	     Question questionPoint2 = new Question();
	     List<Question> questionPointList = new ArrayList<Question>();
	     questionPoint.setQuestion("A");
	     questionPoint.setPoint(50);
	     questionPoint.setQuestionTestA("QuestionTestA");
	     questionPoint.setQuestionTestB("QuestionTestB");
	     questionPoint.setQuestionTestC("QuestionTestC");
	     questionPoint.setQuestionTestD("QuestionTestD");
	     questionPoint.setAnswer("B");
	     questionPointList.add(questionPoint);
	     questionPoint2.setQuestion("B");
	     questionPoint2.setPoint(50);
	     questionPoint2.setQuestionTestA("QuestionTestA");
	     questionPoint2.setQuestionTestB("QuestionTestB");
	     questionPoint2.setQuestionTestC("QuestionTestC");
	     questionPoint2.setQuestionTestD("QuestionTestD");
	     questionPoint2.setAnswer("B");
	     questionPointList.add(questionPoint2);
	     testObject.setName(name);
	     testObject.setQuestion(question);
	     testObject.setQuestionPoint(questionPointList);
	   	 try {
	   		System.out.println(	createTestService.postCreate(testObject) );
	   	 } catch (Exception e) {
			System.out.println("Error" +e.getMessage());
	   	 }
		
	}
	
	@Test
	public void createStudent() {
		StudentObject testObject = new StudentObject();
		testObject.setAge(20);
		testObject.setName("nameTest");
		testObject.setCity("Asia/Hong_Kong");
		testObject.setTimeZone("Asia/Hong_Kong");
	   	 try {
	   		System.out.println(	studentService.postCreateStudent(testObject) );
	   	 } catch (Exception e) {
			System.out.println("Error" +e.getMessage());
	   	 }
		
	}

	
	@Test
	public void asignationTest() {
		AsignationTestObject testObject = new AsignationTestObject();
		testObject.setAge(2022);
		testObject.setMount(9);
		testObject.setDay(15);
		testObject.setHour(12);
		testObject.setMinute(51);
		testObject.setIdTest("1");
	   	 try {
	   		System.out.println(	testApplicationService.postAsignation(testObject) );
	   	 } catch (Exception e) {
			System.out.println("Error" +e.getMessage());
	   	 }
		
	}
	

	@Test
	public void evaluationTest() {
		EvaluationTestObject testObject = new EvaluationTestObject();
		testObject.setIdTestStudent("1");
		List<QuestionAnswerTestObject> questionAnswerTestObjects= new ArrayList<QuestionAnswerTestObject>();
		QuestionAnswerTestObject answerTestObject = new QuestionAnswerTestObject();
		QuestionAnswerTestObject answerTestObject2 = new QuestionAnswerTestObject();
		answerTestObject.setIdQuestion("1");
		answerTestObject.setAsnwer("B");
		answerTestObject2.setIdQuestion("2");
		answerTestObject2.setAsnwer("B");
		questionAnswerTestObjects.add(answerTestObject);
		questionAnswerTestObjects.add(answerTestObject2);
		testObject.setQuestionAnswerTestObjects(questionAnswerTestObjects);
		testObject.setIdTest("1");
	   	 try {
	   		System.out.println(	testApplicationService.postEvaluation(testObject) );
	   	 } catch (Exception e) {
			System.out.println("Error" +e.getMessage());
	   	 }
		
	}
	
	@Test
	public void checkAnswer() {
		QualificatedTestObject testObject = new QualificatedTestObject();
		testObject.setIdTestAEvaluar(1);
		
	   	 try {
	   		System.out.println(	testApplicationService.postAnswer(testObject) );
	   	 } catch (Exception e) {
			System.out.println("Error" +e.getMessage());
	   	 }
		
	}

}
